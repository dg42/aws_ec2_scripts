#!/usr/bin/python
#Python 3.7.2

import boto3
from datetime import datetime, timedelta, timezone

def lambda_handler(event, context):
  terminate_stale_instances('test-kitchen', 4)

def terminate_stale_instances(tag, launchTimeAgo):
  stale_instances = list_ec2_instances(tag, launchTimeAgo)
  terminate_ec2_instances(stale_instances)

def list_ec2_instances(tag, launchTimeAgo):
  client = boto3.client('ec2')
  response = client.describe_instances(
    Filters=[
      {
        'Name': 'tag:' + tag,
        'Values': ['true']
      }
    ],
    DryRun=False
  )
  stale_instances = filter_stale_instances(response, launchTimeAgo)
  return stale_instances

def terminate_ec2_instances(instances):
  ec2 = boto3.resource('ec2')
  for instance_id in instances:
    instance = ec2.Instance(instance_id)
    response = instance.terminate(
      DryRun=False
    )

def filter_stale_instances(response, launchTimeAgo):
  stale_instances = []
  cutoff_time = datetime.now(timezone.utc) - timedelta(hours=launch_time_ago)
  for reservation in response['Reservations']:
    for instance in reservation['Instances']:
      if instance['LaunchTime'] < cutoff_time:
        stale_instances.append(instance['InstanceId'])
  return stale_instances
