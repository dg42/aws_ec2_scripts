#!/usr/bin/python
#Python 3.7.2

#aws_ec2_handler.py
#Author: Dan Gordon
#Modified: 30 January 2019

import boto3
import time
import subprocess
import os
import sys

# TODO: variablize this
boto3.setup_default_session(profile_name='dg')

class EC2Handler:


    def __init__(self, instance_id):
        self.instance_id = instance_id
        self.instance_state = 0
        self.public_dns_name = ''
        self.platform = os.name
        EC2Handler.STATE_RUNNING = 16
        EC2Handler.STATE_STOPPED = 80


    def get_instance_state(self):
        ec2 = boto3.resource('ec2')
        instance = ec2.Instance(self.instance_id)
        self.instance_state = instance.state["Code"]
        if self.instance_state == EC2Handler.STATE_RUNNING:
            self.public_dns_name = instance.public_dns_name


    def start_instance(self, user, putty_session='', putty_path='C:\Program Files\PuTTY\putty.exe'):
        self.user = user
        self.putty_session = putty_session
        self.putty_path = putty_path
        instance_ids = [self.instance_id]
        client = boto3.client('ec2')
        response = client.start_instances(
            InstanceIds=instance_ids,
            DryRun=False
        )
        if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
            while True:
                self.get_instance_state()
                if (self.instance_state == EC2Handler.STATE_RUNNING):
                    previous_state = response["StartingInstances"][0]["PreviousState"]["Code"]
                    if previous_state != EC2Handler.STATE_RUNNING:
                        time.sleep(10) # the ec2 instances seem to take a while to accept connections when first started
                    if self.platform == 'nt':
                        self.open_putty_session()
                    else:
                        self.open_ssh_session()
                    break
                else:
                    time.sleep(1)
        else:
            print(response)


    def stop_instance(self):
        if (self.platform == 'nt'):
            self.kill_putty_session()
        else:
            self.kill_ssh_session()
        instance_ids = [self.instance_id]
        client = boto3.client('ec2')
        response = client.stop_instances(
            InstanceIds=instance_ids,
            Hibernate=False,
            DryRun=False,
            Force=False
        )
        if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
            while True:
                self.get_instance_state()
                if self.instance_state == EC2Handler.STATE_STOPPED:
                    break
                else:
                    time.sleep(1)
        else:
            print(response)


    def open_putty_session(self):
        command = self.putty_path
        if (self.putty_session != ''):
            command += ' -load ' + self.putty_session
        command += ' ' + self.user + '@' + self.public_dns_name
        pid = subprocess.Popen(command).pid


    def kill_putty_session(self):
        os.system("taskkill /f /im putty.exe")

    
    def open_ssh_session(self):
        command = 'ssh -T -x ' + self.user + '@' + self.public_dns_name
        pid = subprocess.Popen(command, shell=True).pid


    def kill_ssh_session(self):
        command = 'ps ax | grep "ssh -T -x"'
        pid = subprocess.call(command).pid


method = sys.argv[1]
instance_id = sys.argv[2]
handler = EC2Handler(instance_id)
if method == 'start':
    user = sys.argv[3]
    handler.start_instance(user, 'ec2')
elif method == 'stop':
    handler.stop_instance()
else:
    raise ValueError('Invalid argument provided at position 1: valid choices are "start" or "stop".')