#!/usr/bin/python
#Python 3.7.2

import boto3
import requests
import json
import base64

def lambda_handler(event, context):
  test_kitchen_pool_id = 34
  cred = get_azure_dev_ops_creds('/chef/automation/vsts_agent/token')
  delete_test_kitchen_agents(test_kitchen_pool_id, cred)

def delete_test_kitchen_agents(pool_id, cred):
  test_kitchen_agents = get_test_kitchen_agents(pool_id, cred)
  for agent in test_kitchen_agents:
    delete_agent(pool_id, agent, cred)

def get_test_kitchen_agents(pool_id, cred):
  url = f'https://dev.azure.com/apsourcecontrol/_apis/distributedtask/pools/{pool_id}/agents?api-version=5.1-preview.1'
  headers = {'Authorization': cred}
  response = requests.get(url, headers=headers)
  return response.json()['value']

def delete_agent(pool_id, agent, cred):
  agent_id = agent['id']
  headers = {'Authorization': cred}
  url = f'https://dev.azure.com/apsourcecontrol/_apis/distributedtask/pools/{pool_id}/agents/{agent_id}?api-version=5.1-preview.1'
  response = requests.delete(url, headers=headers)

def get_azure_dev_ops_creds(parameter_name):
  client = boto3.client('ssm')
  response = client.get_parameter(
      Name=parameter_name,
      WithDecryption=True
  )
  return b'Basic ' + base64.b64encode((':' + response['Parameter']['Value']).encode())
